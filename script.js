const userScore2 = document.getElementById("userScore");
const comScore2 = document.getElementById("comScore");

const result = document.getElementById("result");

const paper_div = document.getElementById("paper");
const scissors_div = document.getElementById("scissors");
const rock_div = document.getElementById("rock");

let userScore = 0;
let comScore = 0;

userScore2.innerHTML = userScore;
comScore2.innerHTML = comScore;

function getComChoice() {
    const choices = ['rock', 'paper', 'scissors'];
    const randomNumber = Math.floor(Math.random() * 3);
    return choices[randomNumber];
}

function win(user, computer) {
    userScore++;
    userScore2.innerHTML = userScore;
    comScore2.innerHTML = comScore;

    result.innerHTML = user + " beats " + computer + ". You win!";
}

function lose(user, computer) {
    comScore++;

    userScore2.innerHTML = userScore;
    comScore2.innerHTML = comScore;

    result.innerHTML = computer + " beats " + user + ". You lose!";
}

function draw(user, computer) {
    result.innerHTML = user + " draws " + computer + ". It's a draw!";
}

function game(userChoice) {
    const comChoice = getComChoice();
    console.log(userChoice);
    switch (userChoice + comChoice) {
        case "paperrock":
        case "rockscissors":
        case "scissorspaper":
            console.log("USER WINS");
            win(userChoice, comChoice);
            break;

        case "rockpaper":
        case "scissorsrock":
        case "paperscissors":
            console.log("USER LOSES");
            lose(userChoice, comChoice);
            break;
        
        case "rockrock":
        case "paperpaper":
        case "scissorsscissors":
            console.log("DRAW");
            draw(userChoice, comChoice);
            break;
    }
}

rock_div.addEventListener('click', function() {
    game("rock");
});

paper_div.addEventListener('click', function(){
    game("paper");
});

scissors_div.addEventListener('click', function(){
    game("scissors");
});